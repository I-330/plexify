    Plexify is an alternative to the Plexamp linux app developed by Plex. The
    goal of the project is to create a graphical application for streaming plex
    server music on linux with all of the comforts of a typical minimalist music
    player. My current aim is to build a functional terminal based music player
    built on the plex api first before implementing a graphical interface. This
    is really just a project for me, but I will make sure to comment and
    document development as I move forward. 
