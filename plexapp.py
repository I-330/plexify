import requests
import sys
from os import system
import plexapi
import pygame

# Import keys for plex server access
# Key variables include server baseurl and server token
# Both keys are stored as strings in a keys.py file
from keys import *
from plexapi.server import PlexServer
plex = PlexServer(baseurl, token)

# Might end up replacing pygame mixer later
from pygame import mixer
pygame.mixer.init()

# clears the screen (only on unix systems)
def clear():
    _ = system('clear')

clear()

# Create 2D track array for storing download id and track info from search
h,w = 10,4
trackArr = [[0 for x in range(w)] for y in range(h)]

# Searches the plex server music library for albums, artists, and tracks 
def searchMusic():
 
    usrInput = input("Enter a search term : ")

    # Searches the 'Music' Library on defined plex server
    music = plex.library.section('Music')


    print("|====[Artists]====|")
    for i in music.searchArtists(title=usrInput):
        print(i.titleSort)

    print("\n|====[Albums]====|")
    for i in music.searchAlbums(title=usrInput):
        print(i.parentTitle + " - " + i.titleSort)

    print("\n|====[Tracks]====|")

    # a is an int which is incrimented with each track search loop
    a = 0
    
    # Loop through all returned tracks from search.
    for i in music.searchTracks(title=usrInput):
        
        # Print out track info with deliminator
        print(str(a+1) + ": " + i.grandparentTitle + " - " + i.title)
        
        # Set trackID variable in order to later download file
        trackID = i.media[0].parts[0].id

        # Add each trackID to array
        trackArr[a][0] = str(trackID)
        # Add each album title to array
        trackArr[a][1] = str(i.parentTitle)
        # Add each artist title to array
        trackArr[a][2] = str(i.grandparentTitle)
    
        # Incriment a
        a+=1
        
        # Break after 10 loops
        if(a >= 10):
            break

# Loop search/play forever until break. Probably a better way to do this, but I
# am a lazy individual.
while(True):

    searchMusic()

    downloadInput = input("Which do you want to play? [1-10] : ")

    # subtract from downloadInput because track array starts at 0
    trackNum = str(trackArr[int(downloadInput)-1][0])

    # Download selected file as 'track.flac'
    r = requests.get(baseurl + '/library/parts/' + trackNum + '/?download=1&X-Plex-Token=' + token)
    open('./track.flac', 'wb').write(r.content)

    # Play 'track.flac'
    music_file = "./track.flac"
    mixer.music.load(music_file)
    mixer.music.play()

    clear()

    # Controls to play, pause, exit, or search for a new track
    while(True):
        checkInput = input("[p(ause) | e(xit) | s(earch)]")
        if(checkInput=="s"):
            clear()
            break
        if(checkInput=='e'):
            clear()
            sys.exit(0)
        clear()
        mixer.music.pause()
        checkInput = input("[p(lay) | e(xit) | s(earch)]")
        if(checkInput=="s"):
            clear()
            break
        if(checkInput=='e'):
            clear()
            sys.exit(0)
        clear()
        mixer.music.unpause()
    # Stop music if searching for new track
    mixer.music.stop()
