import PySimpleGUI as sg

sg.theme('DarkAmber')

layout = [ [sg.Text('Album Cover Placeholder')],
           [sg.Text('Play / Stop / Pause Placeholder')],
           [sg.Text('Search'), sg.InputText()],
           [sg.Button('Search'), sg.Button('Exit')] ]

window = sg.Window('Plexify', layout)

while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    print('You entered ', values[0])

window.close()
